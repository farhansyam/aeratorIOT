@extends('backend.master')
@section('content')
    <div class="row">
            <div class="col s12">
              <div class="card">
                <div class="card-content">
                  <h5 class="title">Logs Data Monitoring Kolam pada Akun <b>{{auth()->user()->name}}</b></h5>
                  <br>
                </div>
              </div>
            </div>
          </div>
          

          <div class="row">
            @for ($i = 0; $i <count($ref); $i++)
            <div class="col l6 m6 s12">
            <div class="card">
                <div class="card-content">
                    <div class="d-flex align-items-center no-block">
                        <div>
                            <h3 class="font-light">{{$key[$i]}}</h3>
                            <h6>Detail Kolam</h6>
                        </div>
                        <div class="ml-auto">
                            <div data-label="20%" class="css-bar m-b-0 css-bar-primary css-bar-20"><a href="{{url('log/'.$key[$i])}}"><button class="waves-effect waves-light blue btn"><i class="material-icons dp48">remove_red_eye</i></button></a>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
            @endfor
            
        
          </div>
            <div class="fixed-action-btn">
              <a class="btn-floating btn-large blue" href="{{url('kolam/create')}}">
                <i class="large material-icons">add</i>aa
              </a>
            </div>
      
@endsection